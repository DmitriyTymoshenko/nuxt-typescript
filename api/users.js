// we can get data from any DB
async function getDataFromDB() {
  return (await require("axios").get(`https://reqres.in/api/users?page=1`)).data
}

async function list() {
  const res = await getDataFromDB()
  return res.data.sort(() => 0.5 - Math.random())
}

async function user({ id }) {
  const res = await getDataFromDB()
  const user = res.data.find((item) => item.id === +id)
  return user
}

export { list, user }
