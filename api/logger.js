// const Koa = require("koa")
// const app = new Koa()
// const Router = require("koa-router")
// const router = new Router()
// app.use(router.routes()).use(router.allowedMethods())
// router.get("/", (ctx, next) => {
//   // ctx.router available
// })
// router.get("/", (ctx, next) => {
//   ctx.body = "Hello World!"
// })
// const ctrlTask = require("../controller")
const service = require("../service")
module.exports = async function (req, res, next) {
  // req is the Node.js http request object
  // eslint-disable-next-line no-unused-expressions
  try {
    const results = await service.getAllTasks()
    res.render("Hello World")
    // res.json({
    //   status: "success",
    //   code: 200,
    //   data: {
    //     tasks: results,
    //   },
    // })
  } catch (e) {
    console.error(e)
    next(e)
  }

  // res is the Node.js http response object

  // next is a function to call to invoke the next middleware
  // Don't forget to call next at the end if your middleware is not an endpoint!
  next()
}
